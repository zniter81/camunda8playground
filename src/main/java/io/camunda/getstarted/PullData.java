package io.camunda.getstarted;

import io.camunda.zeebe.client.ZeebeClient;

import java.util.Calendar;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PullData {

  private static final Logger LOG = LogManager.getLogger(PullData.class);

  public static void main(String[] args) {
    try (ZeebeClient client = ZeebeClientFactory.getZeebeClient()) {
      client.newWorker().jobType("pullDataFromGoogle").handler((jobClient, job) -> {
        final String message_content = (String) job.getVariablesAsMap().get("message_content");

        LOG.info("Sending email with message content: {}", message_content);

        Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
        calendar.add(Calendar.SECOND, 5);
        LOG.info(calendar.getTime());
        throw new Exception("");



      }).open();

      // run until System.in receives exit command
      waitUntilSystemInput("exit");
    }
  }

  private static void waitUntilSystemInput(final String exitCode) {
    try (final Scanner scanner = new Scanner(System.in)) {
      while (scanner.hasNextLine()) {
        final String nextLine = scanner.nextLine();
        if (nextLine.contains(exitCode)) {
          return;
        }
      }
    }
  }
}
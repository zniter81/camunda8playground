package io.camunda.getstarted;

import io.camunda.zeebe.client.ZeebeClient;
import io.camunda.zeebe.client.api.response.ProcessInstanceEvent;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DeployAndStartInstance {

  private static final Logger LOG = LogManager.getLogger(DeployAndStartInstance.class);

  public static void main(String[] args) {
    try (ZeebeClient client = ZeebeClientFactory.getZeebeClient()) {
      client.newDeployResourceCommand()
          .addResourceFromClasspath("send-email.bpmn")
          .send()
          .join();

      for(int x=0; x<100; x++) {
        final ProcessInstanceEvent event = client.newCreateInstanceCommand()
                .bpmnProcessId("send-email")
                .latestVersion()
                .variables(Map.of("message_content", "Hello from the Java get started"))
                .send()
                .join();
      }
    }
  }
}